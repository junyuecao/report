# 报告生成

## 1.安装步骤

 1. 安装node.js [http://nodejs.org](http://nodejs.org)
 2. 进入report文件夹根目录，运行`npm install`
 3. 进入report文件夹根目录，运行`node app`
 4. 打开浏览器，输入[http://localhost:3000](http://localhost:3000)

## 2.说明
 1. 上传的图片位于`/public/images`目录，可定期清理